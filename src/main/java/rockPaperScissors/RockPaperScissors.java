package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class RockPaperScissors {

	public static void main(String[] args) {
    	/*
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public String computer_choice() {
        //Computers choice. Randomly generates either option 1, 2 or 3.
        int rand = (int) (Math.random() * 3);
        String compMove;
        if (rand == 0) {
            compMove = "rock";
        } else if (rand == 1) {
            compMove = "paper";
        } else {
            compMove = "scissors";
        }

        return compMove;
    }
    public String user_choice() {
        /*
        Prompt the user with what choice of rock paper scissors they choose
        :return:"rock", "paper" or "scissors"
        */
        while(true) {
            String myMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validate_input(myMove, rpsChoices)) {
                return myMove;
            } else {
                System.out.printf("I don't understand %s. Could you try again?\n", myMove);
            }
        }
    }

    public Boolean is_winner(String user, String opponent){
        //Calculates if the user wins, loses or if it's a tie.
        if (user.equals("paper")) {
            return opponent.equals("rock");
        }else if (user.equals("scissors")) {
            return opponent.equals("paper");
        } else {
            return opponent.equals("scissors");
        }
    }

    public Boolean validate_input(String input, List<String> valid_input) {
            /*
            Checks if the given input is either rock, paper or scissors.
            :param input:user input string
            :
            param valid_input:list of valid input strings
            :
            return:true if valid input, false if not
            */
            String lowerCaseInput = input.toLowerCase();
            return valid_input.contains(lowerCaseInput); //Verifies that myMove is valid
    }
    public String continuePlaying(){
        //Asks player to play again, and verifies understandable answers
        while(true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?");
            if (!continue_answer.equals("y") && !continue_answer.equals("n")) {
                System.out.println("I do not understand " + continue_answer + ". Could you try again?");
            } else {
                return continue_answer;
            }
        }
    }

    public void run() {

        //Using a while(true) loop and only break the loop if the user wants to quit
        while(true) {

            //Welcome message
            System.out.println("Let's play round " + roundCounter);

            //User chooses and the choice is verified
            String myMove = user_choice();
            validate_input(myMove, rpsChoices);
            String computerMove = computer_choice();

            System.out.printf("Human chose %s, computer chose %s. ", myMove, computerMove);

            //Calculates if the user wins, loses or if it's a tie.
            if (is_winner(myMove, computerMove)) {
                System.out.println("Human wins!");
                humanScore += 1;
            } else if (is_winner(computerMove, myMove)) {
                System.out.println("Computer wins!");
                computerScore += 1;
            } else {
                System.out.println("It's a tie!");
            }

            //Prints scoreboard to console
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            //Checks if the game is to be restarted.
            String continue_answer = continuePlaying();

            if (continue_answer.equals("n")) {
                break;
            }

            //Updates round_counter
            roundCounter += 1;

        }


        //The game ends
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
